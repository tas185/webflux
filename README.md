**Example using.**

1. Example Login : this is the case of a successful login

![login](/uploads/61f1aa30671d1871b7cc59d4bbf1e4ef/login.JPG)

2. Example Login : this is the case when the user enters his password wrong

![IncorrectaClave](/uploads/847458a4b80f5288566d7b596cd89282/IncorrectaClave.JPG)

3. Example Login : this is the case when trying to access without credentials 

![accesoProhibido](/uploads/1dfb73ed81c63ac07a37e820f08ca777/accesoProhibido.JPG)

**Exampe Get Anotation EndPoint to Course**

> -in this case it is listed by age in ascending order

![cursos](/uploads/81a533834c15339943c3657749698c96/cursos.JPG)

**Example Get Funtional EndPoint to Course**

![getCursov2](/uploads/af08c7dca8b373e9fce9ab43fb393bf8/getCursov2.JPG)

**Example Get Anotation EndPoint to Student**

![estudiantes](/uploads/5139c682b7279651a6e4b9f6b145be5b/estudiantes.JPG)

**Example Get Functional EndPoint to Student**

![getEstudiantev2](/uploads/e50e39b6a4a2a27ad9a46d49e2b4af17/getEstudiantev2.JPG)

**Example list of enrollment of student.**
> in this case, the enrollment list is returned with the entire collection of elements
![ListaMatricula](/uploads/3d4f55c5c2a46e4b46ec859046382bbc/ListaMatricula.JPG)



