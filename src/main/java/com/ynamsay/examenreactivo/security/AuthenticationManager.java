package com.ynamsay.examenreactivo.security;

import java.util.List;
import java.util.stream.Collectors;

import io.jsonwebtoken.Claims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

//Clase S5
@Component
public class AuthenticationManager implements ReactiveAuthenticationManager {

	Logger logger = LoggerFactory.getLogger(SecurityContextRepository.class);
	@Autowired
	private JWTUtil jwtUtil;

	@Override
	public Mono<Authentication> authenticate(Authentication authentication) {
		String token = authentication.getCredentials().toString();

//		logger.info("me ejecuto primero");
		String usuario;
		try {
			usuario = jwtUtil.getUsernameFromToken(token);
		} catch (Exception e) {
			usuario = null;
		}

		if (usuario != null && jwtUtil.validateToken(token)) {
			Claims claims = jwtUtil.getAllClaimsFromToken(token);

			List<String> rolesMap = claims.get("roles", List.class);

			UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
				usuario,
				null,
				rolesMap.stream().map(authority -> new SimpleGrantedAuthority(authority)).collect(Collectors.toList())
			);
			return Mono.just(auth);
		}else {
//			return Mono.empty();
			return Mono.error(new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE));
		}
	}
}
