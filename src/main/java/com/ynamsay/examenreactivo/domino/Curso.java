package com.ynamsay.examenreactivo.domino;

import com.mongodb.lang.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotEmpty;

@Document(collection = "cursos")
public class Curso {
    @Id
    private String id;


    @NotEmpty
    @Field(name="nombre")
    private String nombre;
    @Field(name="sigla")
    private String sigla;
    @NonNull
    @Field(name="estado")
    private Boolean estado;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @NonNull
    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(@NonNull Boolean estado) {
        this.estado = estado;
    }
}
