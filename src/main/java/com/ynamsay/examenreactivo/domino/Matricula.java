package com.ynamsay.examenreactivo.domino;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ynamsay.examenreactivo.util.ItemEstudianteSerializer;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Document(collection = "matriculas")
public class Matricula {
    @Id
    private String id;

    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    @Field(name="fecha")
    private LocalDateTime fechaMatri= LocalDateTime.now();


    private boolean estado;

    @JsonSerialize(using= ItemEstudianteSerializer.class)
    private Estudiante estudiante;

    private List<MatriculaItem> items;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getFechaMatri() {
        return fechaMatri;
    }

    public void setFechaMatri(LocalDateTime fechaMatri) {
        this.fechaMatri = fechaMatri;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public List<MatriculaItem> getItems() {
        return items;
    }

    public void setItems(List<MatriculaItem> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Matricula{" +
                "id='" + id + '\'' +
                ", fechaMatri=" + fechaMatri +
                ", estado=" + estado +
                ", estudiante=" + estudiante +
                ", items=" + items +
                '}';
    }
}
/*
* El enfoque mas adecuado para enlazar con estudiante es usar un objeto tipo estudiante;
*
*
* dbRef= referencia a la coleccion atravez de llave primaria
*
* dbref spring boot 2.3 hacia arriba no soporta
*
* recomienda almacenarlo sin dbref y con un consulta traer los datos que necesitas
*
*
* dbref trae problemas en performance
*
* la representacion de item no necesariamente la clase item va ser representada en la coleccion
* como es una clase de apoyo no se necesita anotarla.
* */
