package com.ynamsay.examenreactivo.domino;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ynamsay.examenreactivo.util.ItemCursoSerializer;

public class MatriculaItem {

    @JsonSerialize(using= ItemCursoSerializer.class)
    private Curso curso;

    public MatriculaItem(){

    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }
}
