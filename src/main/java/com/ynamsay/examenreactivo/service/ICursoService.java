package com.ynamsay.examenreactivo.service;

import com.ynamsay.examenreactivo.domino.Curso;

public interface ICursoService extends ICRUD<Curso,String> {
}
