package com.ynamsay.examenreactivo.service;

import com.ynamsay.examenreactivo.domino.Matricula;
import reactor.core.publisher.Flux;

public interface IMatriculaService extends ICRUD<Matricula,String> {

    Flux<Matricula> getAllMatriculaWithEstudiante();
}
