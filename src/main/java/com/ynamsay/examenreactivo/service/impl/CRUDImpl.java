package com.ynamsay.examenreactivo.service.impl;

import com.ynamsay.examenreactivo.PageSupport;
import com.ynamsay.examenreactivo.repositorio.IGenericRepository;
import com.ynamsay.examenreactivo.service.ICRUD;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;

public abstract class  CRUDImpl<T,ID> implements ICRUD<T,ID> {

    /*  Este metodo se va comportar de manera polimorfica cuando sea llamado
     */
    protected abstract IGenericRepository<T,ID> getRepo();

    @Override
    public Mono<T> registrar(T t) {
        return getRepo().save(t);
    }

    @Override
    public Mono<T> modificar(T t) {
        return getRepo().save(t);
    }

    @Override
    public Flux<T> listar() {
        return getRepo().findAll();
    }

    @Override
    public Mono<T> listarPorId(ID id) {
        return getRepo().findById(id);
    }

    @Override
    public Mono<Void> eliminar(ID id) {
        return getRepo().deleteById(id);
    }

    @Override
    public Mono<PageSupport<T>> litarPage(Pageable page) {
        return getRepo().findAll()
                .collectList()
                .map(list -> new PageSupport<>(
                        list
                        .stream()
                        .skip(page.getPageNumber()*page.getPageSize())
                        .limit(page.getPageSize())
                        .collect(Collectors.toList()),
                        page.getPageNumber(),page.getPageSize(),list.size()
                ));

        //golpenado a la base de datos
        //db.estudiantes.find().skip(5).limit(5);

        //reemplezar el findAll por un query particular a la bd
    }
}
