package com.ynamsay.examenreactivo.service.impl;

import com.ynamsay.examenreactivo.PageSupport;
import com.ynamsay.examenreactivo.domino.Estudiante;
import com.ynamsay.examenreactivo.domino.Matricula;
import com.ynamsay.examenreactivo.repositorio.IGenericRepository;
import com.ynamsay.examenreactivo.repositorio.IMatriculaRepository;
import com.ynamsay.examenreactivo.service.IEstudianteService;
import com.ynamsay.examenreactivo.service.IMatriculaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;

@Service
public class MatriculaImpl extends CRUDImpl<Matricula,String> implements IMatriculaService {

    Logger LOG = LoggerFactory.getLogger(MatriculaImpl.class);
    @Autowired
    private IMatriculaRepository iMatriculaRepository;
    @Autowired
    private IEstudianteService iEstudianteService;

    @Override
    protected IGenericRepository<Matricula, String> getRepo() {
        return iMatriculaRepository;
    }

    @Override
    public Flux<Matricula> getAllMatriculaWithEstudiante() {

       Flux<Matricula> demo= (Flux<Matricula>) getRepo().findAll().flatMap(f ->{
            return Mono.just(f)
                    .zipWith(iEstudianteService.listarPorId(f.getEstudiante().getId()),(fa,es) -> {
                        fa.setEstudiante(es);
                        return fa;
                    });
        });

        return demo;
    }
}
