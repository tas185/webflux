package com.ynamsay.examenreactivo.service.impl;

import com.ynamsay.examenreactivo.domino.Curso;
import com.ynamsay.examenreactivo.repositorio.ICursoRepository;
import com.ynamsay.examenreactivo.repositorio.IGenericRepository;
import com.ynamsay.examenreactivo.service.ICRUD;
import com.ynamsay.examenreactivo.service.ICursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class CursoImpl extends CRUDImpl<Curso,String> implements ICursoService {

    @Autowired
    private ICursoRepository iCursoRepository;

    @Override
    protected IGenericRepository<Curso, String> getRepo() {
        return iCursoRepository;
    }

}
