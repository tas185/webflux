package com.ynamsay.examenreactivo.service.impl;

import com.ynamsay.examenreactivo.domino.Usuario;
import com.ynamsay.examenreactivo.repositorio.IGenericRepository;
import com.ynamsay.examenreactivo.repositorio.IRolRepository;
import com.ynamsay.examenreactivo.repositorio.IUsuarioRepository;
import com.ynamsay.examenreactivo.security.User;
import com.ynamsay.examenreactivo.service.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
@Service
public class UsuarioServiceImpl extends CRUDImpl<Usuario,String> implements IUsuarioService {

    @Autowired
    private IUsuarioRepository iUsuarioRepository;
    @Autowired
    private IRolRepository iRolRepository;

    @Override
    protected IGenericRepository<Usuario, String> getRepo() {
        return iUsuarioRepository;
    }


    @Override
    public Mono<User> buscarPorUsuario(String usuario) {

        Mono<Usuario> usuarioMono = iUsuarioRepository.findOneByUsuario(usuario);

        List<String > roles= new ArrayList<String>();

        return usuarioMono.flatMap(u-> {
            return Flux.fromIterable(u.getRoles())
                    .flatMap(rol -> {
                        return iRolRepository.findById(rol.getId())
                                .map(r -> {
                                    roles.add(r.getNombre());
                                    return r;
                                });
                    }).collectList().flatMap(list -> {
                        u.setRoles(list);
                        return Mono.just(u);
                    });
        })
                .flatMap(u -> {
                    return Mono.just(new User(u.getUsuario(),u.getClave(),u.getEstado(),roles));
                });

    }
}
