package com.ynamsay.examenreactivo.service.impl;

import com.ynamsay.examenreactivo.domino.Estudiante;
import com.ynamsay.examenreactivo.repositorio.IEstudianteRepository;
import com.ynamsay.examenreactivo.repositorio.IGenericRepository;
import com.ynamsay.examenreactivo.service.ICRUD;
import com.ynamsay.examenreactivo.service.IEstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class EstudianteImpl extends CRUDImpl<Estudiante,String> implements IEstudianteService {


    @Autowired
    private IEstudianteRepository iEstudianteRepository;

    @Override
    protected IGenericRepository<Estudiante, String> getRepo() {
        return iEstudianteRepository;
    }

    @Override
    public Flux<Estudiante> findAllOrderByEdad() {
//        return null;
        return iEstudianteRepository.getAllByEdadNotNullOrderByEdadAsc();
    }
}

/*
* la instancia iEstudianteRepository sera proporcionada
* por el metodo getRepo
* por la herencia de CRUDImpl se puede resolver getRepo
* CRUDImpl es una clase abstracta que tiene la funcionalidad principal crud
* */
