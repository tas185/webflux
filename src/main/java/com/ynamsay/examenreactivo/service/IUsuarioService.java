package com.ynamsay.examenreactivo.service;

import com.ynamsay.examenreactivo.domino.Usuario;
import com.ynamsay.examenreactivo.security.User;
import reactor.core.publisher.Mono;

public interface IUsuarioService extends ICRUD<Usuario,String> {
    Mono<User> buscarPorUsuario(String usuario);
}
