package com.ynamsay.examenreactivo.service;

import com.ynamsay.examenreactivo.domino.Rol;

public interface IRolService extends ICRUD<Rol,String> {
}
