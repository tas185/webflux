package com.ynamsay.examenreactivo.service;

import com.ynamsay.examenreactivo.domino.Estudiante;
import com.ynamsay.examenreactivo.repositorio.IEstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

public interface IEstudianteService extends ICRUD<Estudiante,String> {

        Flux<Estudiante> findAllOrderByEdad();

}

/*
* Si por alguna razon se necesita alguna funcionalidad se tiene esta clase para definir
* */
