package com.ynamsay.examenreactivo;

import com.fasterxml.jackson.annotation.JsonProperty;
import net.minidev.json.annotate.JsonIgnore;

import java.util.List;

public class PageSupport<T> {
    public static final String FIRST_PAGE_NUM="0";
    public  static final String DEFAULT_PAGe_SIZE="20";

    private List<T> content;
    private int pageNumber;
    private int pagesize;
    private long totalElement;

    public  PageSupport(){

    }

    public PageSupport(List<T> content, int pageNumber, int pagesize, long totalElement) {
        this.content = content;
        this.pageNumber = pageNumber;
        this.pagesize = pagesize;
        this.totalElement = totalElement;
    }
    @JsonProperty
    public boolean first(){
        return pageNumber==Integer.parseInt(FIRST_PAGE_NUM);
    }

    @JsonProperty
    public  boolean last(){
        return (pageNumber+1) * pagesize>= totalElement;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public long getTotalElement() {
        return totalElement;
    }

    public void setTotalElement(long totalElement) {
        this.totalElement = totalElement;
    }
}
