package com.ynamsay.examenreactivo.controller.anotacion;

import com.ynamsay.examenreactivo.domino.Estudiante;
import com.ynamsay.examenreactivo.domino.Matricula;
import com.ynamsay.examenreactivo.service.IMatriculaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/matriculas")
public class MatriculaController {

    Logger LOG = LoggerFactory.getLogger(MatriculaController.class);
    @Autowired
    private IMatriculaService iMatriculaService;

    @GetMapping
    public Mono<ResponseEntity<Flux<Matricula>>> listar(){

        Flux<Matricula> fxEstudiante = iMatriculaService.listar();

        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(fxEstudiante));
        /*
         * generamos un ResponseEntity con response 200
         * cuyo contenido en json y body es el flujo de Curso pero expresado en mono
         * */
    }

    @GetMapping("/demo")
    public Mono<ResponseEntity<Flux<Matricula>>> listaPersonalizada(){

        LOG.info("usando demo");
        Flux<Matricula> demo= iMatriculaService.getAllMatriculaWithEstudiante();
// Funciona ok
//        demo.subscribe(r->LOG.info(r.toString()));


//        demo.subscribe(data-> LOG.info("valor {}",data.toString()));
//        demo.collectList().subscribe(data-> LOG.info("valor 02 >",data.toString()));


        //                demo.doOnNext(lista->{
//                    Mono<Estudiante> ee;
//                    ee=Mono.just(lista.getEstudiante());
//                    ee.subscribe(data->data.setNombre("juan"));
//                }).subscribe(data->LOG.info("valores:",data.getEstudiante()));


        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(demo));
    }

    @GetMapping("/{id}")
    public  Mono<ResponseEntity<Matricula>> listarPorId(@PathVariable("id") String id){
        return iMatriculaService.listarPorId(id)        //Mono<Curso>
                .map(p-> ResponseEntity.ok()                    //Mono<ResponseEntity>
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Mono<ResponseEntity<Matricula>> registrar(@RequestBody Matricula matricula,final ServerHttpRequest req){
        return iMatriculaService.registrar(matricula) //req.getURI().toString().concat("/").concat(p.getId())
                .map(p-> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p));
    }

    @PutMapping
    public Mono<ResponseEntity<Matricula>> modificar(@RequestBody Matricula matricula){
        return iMatriculaService.modificar(matricula)
                .map(p-> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p));
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id){

        return iMatriculaService.listarPorId(id)
                .flatMap(p->{
                    return iMatriculaService.eliminar(p.getId())
                            .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
                })
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
    }
}
