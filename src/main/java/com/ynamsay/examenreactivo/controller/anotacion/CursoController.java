package com.ynamsay.examenreactivo.controller.anotacion;

import com.ynamsay.examenreactivo.domino.Curso;
import com.ynamsay.examenreactivo.service.ICursoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;



import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;


import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;


@RestController
@RequestMapping("/cursos")
public class CursoController {

        Logger LOG = LoggerFactory.getLogger(CursoController.class);

        @Autowired
        private ICursoService iCursoService;

        @GetMapping
        public Mono<ResponseEntity<Flux<Curso>>> listar(){

            Flux<Curso> fxEstudiante = iCursoService.listar();

            return Mono.just(ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(fxEstudiante));
            /*
             * generamos un ResponseEntity con response 200
             * cuyo contenido en json y body es el flujo de Curso pero expresado en mono
             * */
        }

        @GetMapping("/{id}")
        public  Mono<ResponseEntity<Curso>> listarPorId(@PathVariable("id") String id){
            return iCursoService.listarPorId(id)        //Mono<Curso>
                    .map(p-> ResponseEntity.ok()                    //Mono<ResponseEntity>
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(p)
                    )
                    .defaultIfEmpty(ResponseEntity.notFound().build());
        }

        @PostMapping
        public Mono<ResponseEntity<Curso>> registrar(@Valid @RequestBody Curso curso,final ServerHttpRequest req){
            return iCursoService.registrar(curso) //req.getURI().toString().concat("/").concat(p.getId())
                    .map(p-> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(p));
        }

        @PutMapping
        public Mono<ResponseEntity<Curso>> modificar(@Valid @RequestBody Curso curso){
            LOG.info("PUT:",curso);
            return iCursoService.modificar(curso)
                    .map(p-> ResponseEntity.ok()
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(p));
        }

        @DeleteMapping("/{id}")
        public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id){

            return iCursoService.listarPorId(id)
                    .flatMap(p->{
                        return iCursoService.eliminar(p.getId())
                                .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
                    })
                    .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
        }

        //    uso de hateosas
        @GetMapping("/hateoas/{id}")
        public Mono<EntityModel<Curso>> listarHateoasPorId(@PathVariable("id") String id){
            Mono<Link> link1=linkTo(methodOn(com.ynamsay.examenreactivo.controller.anotacion.EstudianteController.class).listarPorId(id)).withSelfRel().toMono();

            //ideal
            return iCursoService.listarPorId(id)
                    .zipWith(link1,(p,link) -> EntityModel.of(p,link));

        }


}
