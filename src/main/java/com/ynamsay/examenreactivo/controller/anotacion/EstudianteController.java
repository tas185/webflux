package com.ynamsay.examenreactivo.controller.anotacion;


import com.ynamsay.examenreactivo.PageSupport;
import com.ynamsay.examenreactivo.domino.Estudiante;
import com.ynamsay.examenreactivo.service.IEstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {

    @Autowired
    private IEstudianteService iEstudianteService;

    @GetMapping
    public Mono<ResponseEntity<Flux<Estudiante>>> listar(){

//        Flux<Estudiante> fxEstudiante = iEstudianteService.listar();
        Flux<Estudiante> fxEstudiante = iEstudianteService.findAllOrderByEdad();

        return Mono.just(ResponseEntity.ok()
        .contentType(MediaType.APPLICATION_JSON)
        .body(fxEstudiante));
        /*
        * generamos un ResponseEntity con response 200
        * cuyo contenido en json y body es el flujo de estudiante pero expresado en mono
        * */
    }

    @GetMapping("/{id}")
    public  Mono<ResponseEntity<Estudiante>> listarPorId(@PathVariable("id") String id){
        return iEstudianteService.listarPorId(id)        //Mono<Estudiante>
        .map(p-> ResponseEntity.ok()                    //Mono<ResponseEntity>
        .contentType(MediaType.APPLICATION_JSON)
                .body(p)
        )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Mono<ResponseEntity<Estudiante>> registrar(@RequestBody Estudiante estudiante,final ServerHttpRequest req){
        return iEstudianteService.registrar(estudiante) //req.getURI().toString().concat("/").concat(p.getId())
                .map(p-> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
                .contentType(MediaType.APPLICATION_JSON)
                .body(p));
    }

    @PutMapping()
    public Mono<ResponseEntity<Estudiante>> modificar(@Valid @RequestBody Estudiante estudiante){
        return iEstudianteService.modificar(estudiante)
                .map(p-> ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(p));
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id){

        return iEstudianteService.listarPorId(id)
                .flatMap(p->{
                    return iEstudianteService.eliminar(p.getId())
                            .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
                })
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
    }

//    uso de hateosas
    @GetMapping("/hateoas/{id}")
    public Mono<EntityModel<Estudiante>> listarHateoasPorId(@PathVariable("id") String id){
        Mono<Link> link1=linkTo(methodOn(EstudianteController.class).listarPorId(id)).withSelfRel().toMono();

        //ideal
        return iEstudianteService.listarPorId(id)
                .zipWith(link1,(p,link) -> EntityModel.of(p,link));

    }

    @GetMapping("/pageable")
    public  Mono<ResponseEntity<PageSupport<Estudiante>>> litarPageable(
            @RequestParam(name = "page",defaultValue = "0") int page,
            @RequestParam(name="size", defaultValue = "10") int size
    ){

        //Importar de : springframework-data-domain
        Pageable pageRequest = PageRequest.of(page,size);


        return iEstudianteService.litarPage(pageRequest)
                .map(p-> ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(p))
                .defaultIfEmpty(ResponseEntity.notFound().build());

    }


}

/*
* Anotaciones
*
*
* Siempre retorna un mono = peticion http
*
* automaticamente esta el subcribe
*
* Flux<Estudiante> -> Mono
*
* operador reactivo que genera un Mono de la nada (just)
*
* entre llamadas a base de datos usar flatmap
*
* para ver documentado el servicio: localhost:8080/api-docs
* pero se tiene que agregar una clase de configuracion
*
* */
