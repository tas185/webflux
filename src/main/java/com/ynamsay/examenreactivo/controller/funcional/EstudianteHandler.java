package com.ynamsay.examenreactivo.controller.funcional;

import com.ynamsay.examenreactivo.domino.Estudiante;
import com.ynamsay.examenreactivo.service.IEstudianteService;
import com.ynamsay.examenreactivo.validator.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

@Component
public class EstudianteHandler {

    @Autowired
    private IEstudianteService iEstudianteService;

    @Autowired
    private RequestValidator requestValidator;

    public Mono<ServerResponse> listar(ServerRequest r){
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(iEstudianteService.listar(), Estudiante.class);
    }

    public Mono<ServerResponse> listarPorId(ServerRequest r){

        String id=r.pathVariable("id");
        return iEstudianteService.listarPorId(id)
                .flatMap(p-> ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(fromValue(p))
                .switchIfEmpty(ServerResponse.notFound().build())
                );
    }

    public Mono<ServerResponse> registrar(ServerRequest r){

        Mono<Estudiante> estudianteMono= r.bodyToMono(Estudiante.class);

        //antes de registrar se va a realizar la validarion
        //usando metodo a referencia
        //.flatMap(requestValidator :: validate)
        return estudianteMono
                .flatMap(p -> this.requestValidator.validate(p))
                .flatMap(iEstudianteService::registrar)
                .flatMap(p-> ServerResponse.created(URI.create(r.uri().toString().concat("/").concat(p.getId())))
                .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(p))
                );
    }

    public Mono<ServerResponse> modificar(ServerRequest r){
        Mono<Estudiante> estudianteMono= r.bodyToMono(Estudiante.class);
        return estudianteMono.flatMap(iEstudianteService::modificar)
                .flatMap(p-> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(p))
                );
    }

    public Mono<ServerResponse> eliminar(ServerRequest r){
        String id= r.pathVariable("id");

        return iEstudianteService.listarPorId(id)
                .flatMap(p-> iEstudianteService.eliminar(p.getId())
                        .then(ServerResponse.noContent().build()) //ya elimino y devuelvo un mono ServerResponse
                .switchIfEmpty(ServerResponse.notFound().build()));//si nunca encontro el id
    }
}

/*
* Component para utilitarios
*
* siempre como request es el parametro de la aplicacion
*
* fromValue :permite englobar desde from value
* */
