package com.ynamsay.examenreactivo.controller.funcional;

import com.ynamsay.examenreactivo.domino.Matricula;
import com.ynamsay.examenreactivo.service.IMatriculaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

@Component
public class MatriculaHandler {

    @Autowired
    private IMatriculaService iMatriculaService;

    public Mono<ServerResponse> listar(ServerRequest r){
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(iMatriculaService.getAllMatriculaWithEstudiante(), Matricula.class);
    }

    public Mono<ServerResponse> listarPorId(ServerRequest r){

        String id=r.pathVariable("id");
        return iMatriculaService.listarPorId(id)
                .flatMap(p-> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(p))
                        .switchIfEmpty(ServerResponse.notFound().build())
                );
    }

    public Mono<ServerResponse> registrar(ServerRequest r){

        Mono<Matricula> cursoMono= r.bodyToMono(Matricula.class);
        return cursoMono.flatMap(iMatriculaService::registrar)
                .flatMap(p-> ServerResponse.created(URI.create(r.uri().toString().concat("/").concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(p))
                );
    }

    public Mono<ServerResponse> modificar(ServerRequest r){
        Mono<Matricula> cursoMono= r.bodyToMono(Matricula.class);
        return cursoMono.flatMap(iMatriculaService::modificar)
                .flatMap(p-> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(p))
                );
    }

    public Mono<ServerResponse> eliminar(ServerRequest r){
        String id= r.pathVariable("id");

        return iMatriculaService.listarPorId(id)
                .flatMap(p-> iMatriculaService.eliminar(p.getId())
                        .then(ServerResponse.noContent().build()) //ya elimino y devuelvo un mono ServerResponse
                        .switchIfEmpty(ServerResponse.notFound().build()));//si nunca encontro el id
    }
}
