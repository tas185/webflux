package com.ynamsay.examenreactivo.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.ynamsay.examenreactivo.domino.Estudiante;

import java.io.IOException;

public class ItemEstudianteSerializer extends StdSerializer<Estudiante> {

    public ItemEstudianteSerializer()
    {
        this(null);
    }

    public ItemEstudianteSerializer(Class<Estudiante> t) {
        super(t);
    }

    @Override
    public void serialize(Estudiante value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("id",value.getId());
        gen.writeStringField("nombre",value.getApellido() +" "+ value.getNombre());
        gen.writeEndObject();
    }
}
