package com.ynamsay.examenreactivo.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.ynamsay.examenreactivo.domino.Curso;

import java.io.IOException;

public class ItemCursoSerializer  extends StdSerializer<Curso> {

    public ItemCursoSerializer()
    {
        this(null);
    }

    public ItemCursoSerializer(Class<Curso> t) {
        super(t);
    }

    @Override
    public void serialize(Curso value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("id",value.getId());
        gen.writeEndObject();
    }
}
