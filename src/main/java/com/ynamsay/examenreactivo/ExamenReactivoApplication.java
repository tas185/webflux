package com.ynamsay.examenreactivo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenReactivoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamenReactivoApplication.class, args);
    }

}
