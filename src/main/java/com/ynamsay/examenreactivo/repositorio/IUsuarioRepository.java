package com.ynamsay.examenreactivo.repositorio;

import com.ynamsay.examenreactivo.domino.Usuario;
import reactor.core.publisher.Mono;

public interface IUsuarioRepository extends IGenericRepository<Usuario,String> {

    Mono<Usuario> findOneByUsuario(String u);
}
