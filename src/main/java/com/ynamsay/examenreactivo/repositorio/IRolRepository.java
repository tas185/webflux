package com.ynamsay.examenreactivo.repositorio;

import com.ynamsay.examenreactivo.domino.Rol;

public interface IRolRepository extends IGenericRepository<Rol,String> {
}
