package com.ynamsay.examenreactivo.repositorio;

import com.ynamsay.examenreactivo.domino.Matricula;

public interface IMatriculaRepository extends IGenericRepository<Matricula,String> {
}
