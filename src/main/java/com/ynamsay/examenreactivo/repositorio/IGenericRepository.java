package com.ynamsay.examenreactivo.repositorio;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IGenericRepository<T,ID> extends ReactiveMongoRepository<T,ID> {
}

/*
*
* interface generica de apoyo
*
* usamos no repositorybena solo sirve como un abstraccion no sirve para hacer coneccion con bd
*es una interfaz reactiva al usar ReactiveMongoRepository
* */
