package com.ynamsay.examenreactivo.repositorio;

import com.ynamsay.examenreactivo.domino.Estudiante;
import reactor.core.publisher.Flux;

public interface IEstudianteRepository extends IGenericRepository<Estudiante,String> {
    Flux<Estudiante> getAllByEdadNotNullOrderByEdadAsc();
}

/*
*
* esta interface va necesitar de igenericRepository
*
* una interfaz puede heredar de otra interfaz
*
* interfaz de Estudiante hereda de Generico
*
* */
