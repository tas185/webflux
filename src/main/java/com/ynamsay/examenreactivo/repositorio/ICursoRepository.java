package com.ynamsay.examenreactivo.repositorio;

import com.ynamsay.examenreactivo.domino.Curso;

public interface ICursoRepository extends IGenericRepository<Curso,String> {
}
