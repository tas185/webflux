package com.ynamsay.examenreactivo.config.configurationFuncional;

import com.ynamsay.examenreactivo.controller.funcional.CursoHandler;
import com.ynamsay.examenreactivo.controller.funcional.EstudianteHandler;
import com.ynamsay.examenreactivo.controller.funcional.MatriculaHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class RouterConfig {

    @Bean
    public RouterFunction<ServerResponse> rutasEstudiantes(EstudianteHandler handler){
//        return route(GET("/v2/estudiantes"),req -> handler.listar(req));
        //mejorando con metodos a referencia
        return route(GET("/v2/estudiantes"),handler::listar)
                .andRoute(GET("/v2/estudiantes/{id}"),handler::listarPorId)
                .andRoute(POST(""), handler::registrar)
                .andRoute(PUT("/v2/estudiantes"),handler::modificar)
                 .andRoute(DELETE("/v2/estudiantes/{id}"),handler::eliminar)
                ;
    }

    @Bean
    public RouterFunction<ServerResponse> rutasCursos(CursoHandler handler){
        return route(GET("/v2/cursos"),handler::listar)
                .andRoute(GET("/v2/cursos/{id}"),handler::listarPorId)
                .andRoute(POST(""), handler::registrar)
                .andRoute(PUT("/v2/cursos"),handler::modificar)
                .andRoute(DELETE("/v2/cursos/{id}"),handler::eliminar)
                ;
    }
    @Bean
    public RouterFunction<ServerResponse> rutasMatriculas(MatriculaHandler handler){
        return route(GET("/v2/matriculas"),handler::listar)
                .andRoute(GET("/v2/matriculas/{id}"),handler::listarPorId)
                .andRoute(POST(""), handler::registrar)
                .andRoute(PUT("/v2/matriculas"),handler::modificar)
                .andRoute(DELETE("/v2/matriculas/{id}"),handler::eliminar)
                ;
    }
}

/*
*nos permite expresar nuestras intenciones de navegacion
* cuando hay  Configuration debe haber un bean
*
* con el configuration le decimos a spring que gestione la clase
* */
